import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {


   // https://en.wikipedia.org/wiki/Topological_sorting
   // https://gist.github.com/joaquinvanschoren/5006291
   // https://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/
   // https://www.geeksforgeeks.org/longest-path-in-a-directed-acyclic-graph-dynamic-programming/

   /**
    * Main method.
    */
   public static void main(String[] args) {
      GraphTask a = new GraphTask();

      a.run();
   }

   /**
    * Actual main method to run examples and everything.
    */
   public void run() {
      Graph graph = new Graph("ID");
/*
      long prev = System.currentTimeMillis();
      long now;

      graph.createRandomGraph(2000, 2200);

      now = System.currentTimeMillis();
      long deltaMillis = now - prev;
      System.out.println((int) (deltaMillis / 1000) % 60  + " Seconds.");

      graph.isCyclic();
      graph.printGraph();*/

/*
      graph.createEdge(0, 1);
      graph.createEdge(1, 2);
      graph.createEdge(2, 3);
      graph.createEdge(3, 4);
      graph.createEdge(3, 5);
      graph.createEdge(4, 6);
      graph.createEdge(4, 8);
      graph.createEdge(5, 7);
      graph.createEdge(5, 8);
      graph.createEdge(7, 8);



        graph.createEdge(1, 2);
        graph.createEdge(0, 6);
        graph.createEdge(2, 3);
        graph.createEdge(2, 4);
        graph.createEdge(2, 5);
        graph.createEdge(3, 7);
        graph.createEdge(3, 4);
        graph.createEdge(3, 8);
        graph.createEdge(3, 9);
        graph.createEdge(5, 4);
        graph.createEdge(5, 11);
        graph.createEdge(6, 1);
        graph.createEdge(6, 2);
        graph.createEdge(7, 8);
        graph.createEdge(8, 12);
        graph.createEdge(8, 9);
        graph.createEdge(9, 10);
        graph.createEdge(10, 11);
        graph.createEdge(10, 12);
        graph.createEdge(11, 4);

*/


      graph.createEdge(0, 1);

      // Your edges here

      graph.createEdge(1, 2);





      graph.calculateLongestDistance(0, 1);  // initialize longest path

      graph.printPath(); // print path

      graph.printGraph(); // print graph

      List<Integer> longestPath = graph.getPath(); // get path




   }

   /**
    * Vertex represents one Node in the graph.
    */
   static class Vertex {

      private Integer vertexId;
      private Vertex nextVertex;
      private Arc vertexArc;
      private int info = 0;
      // You can add more fields, if needed

      Vertex(Integer verId, Vertex nextVer, Arc arc) {
         vertexId = verId;
         nextVertex = nextVer;
         vertexArc = arc;
      }

      Vertex(Integer vertexId) {
         this(vertexId, null, null);
      }

      @Override
      public String toString() {
         return String.valueOf(vertexId);
      }

   }


   /**
    * Arc represents one arrow in the graph.
    */
   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

   }


   static class Graph {

      private List<Integer> longestPath = new ArrayList<>(); // Can be returned after using calculateLongestDistance() Method

      private static ArrayList<Vertex> vContainer = new ArrayList<>(); // Container for all vertexes

      private static Map<Integer, LinkedList<Integer>> adj = new HashMap<>(); // Container for all Edges
      private String id;
      private static Vertex firstGraphVertex;
      private int info = 0;

      boolean[] discovered; // Used in Topological sort to get all discovered Vertexes
      int[] parent; // Used in Topological sort to remember Parents, for paths
      ArrayDeque<Integer> sort; // Used in Topological sort - sorted deque


      Graph(String s, Vertex v) {
         id = s;
         firstGraphVertex = v;

      }
      Graph(String s) {
         this(s, null);
      }

      /*
       * Print all Graph vertexes with arcs.
       */
      public void printGraph() {
         System.out.println(this.toString());
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuffer sb = new StringBuffer(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = firstGraphVertex;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.vertexArc;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.nextVertex;
         }
         return sb.toString();
      }


      /*
       * Gets the longest path
       * @Return this longest Path
       */
      public List<Integer> getPath() {
         return longestPath;
      }

      /*
       * Prints the longest path
       */
      public void printPath() {
         if(longestPath.get(1) == 0) {
            throw new RuntimeException("cant get EndPoint");
         }
         System.out.print("The longest path [");
         for (int i = 0; i < longestPath.size(); i++) {
            System.out.print(i < longestPath.size() - 1 ? longestPath.get(i) + " -> " : longestPath.get(i) + "]\n");
         }
         System.out.printf("from %d to %d, has length %d", longestPath.get(0), longestPath.get(longestPath.size() - 1), longestPath.size() - 1);
         System.out.println();
      }


      public static Vertex createVertex(Integer vid) {
         Vertex res = new Vertex(vid);
         res.nextVertex = firstGraphVertex;
         firstGraphVertex = res;
         return res;
      }

      public static Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.vertexArc;
         from.vertexArc = res;
         res.target = to;
         return res;
      }



      /*
       * Creates a new edge with 2 parameters.
       * @param from Start Vertex id
       * @param to End Vertex id
       * Creates arc
       *
       */
      public static void createEdge(int from, int to) {
         Vertex fromVertex = null;
         Vertex toVertex = null;
         //
         for (Vertex vertex : vContainer) {
            if (vertex.vertexId.equals(from)) {
               fromVertex = vertex;
            }
            if (vertex.vertexId.equals(to)) {
               toVertex = vertex;
            }
         }
         if (fromVertex == null) {
            fromVertex = createVertex(from);
            adj.put(from, new LinkedList<>());
            vContainer.add(fromVertex);
         }
         if (toVertex == null) {
            toVertex = createVertex(to);
            adj.put(to, new LinkedList<>());
            vContainer.add(toVertex);
         }

         adj.get(from).add(to);
         String aId = from + "_" + to;
         createArc(aId, fromVertex, toVertex);

         if (isCyclic())
            throw new RuntimeException("Graph contains cycle " + "(" + from + " -> " + to + ")");

      }


      /*
       * Recursive method what finds cycles in Graph.
       * @return boolean
       * @param i edge
       * @param visited container to check visited edges
       * @param recStack container to check cycle
       */
      private static boolean isCyclicUtil(int i, boolean[] visited,
                                          boolean[] recStack) {
         if (adj.get(i) == null || !(adj.containsKey(i))) {
            return false;
         }

         // Mark the current node as visited and
         // part of recursion stack
         if (recStack[i])
            return true;   // got cycle

         if (visited[i])
            return false;

         visited[i] = true;
         recStack[i] = true;
         List<Integer> children = adj.get(i);

         for (Integer c : children)
            if (isCyclicUtil(c, visited, recStack))
               return true;

         recStack[i] = false;

         return false;
      }

      /*
       * Method for finding cycle in graph.
       * Uses recursive sub method isCyclicUtil()
       * @return true if the graph contains a
       * cycle, else false.
       */
      private static boolean isCyclic() {
         int V = Collections.max(adj.keySet()) + 1;
         // Mark all the vertices as not visited and
         // not part of recursion stack
         boolean[] visited = new boolean[V];
         boolean[] recStack = new boolean[V];

         // Call the recursive helper function to
         // detect cycle in different DFS trees
         for (int i = 0; i < V + 1; i++) {
            if (isCyclicUtil(i, visited, recStack)) {
               return true;
            }
         }

         return false;
      }



      /*
       * Topological sort based on depth-first search
       * @param start,
       */
      public void depthFirstSearch(int start) {
         // Mark the current node as discovered
         discovered[start] = true;
         try {
            for (int node : adj.get(start)) {
               if (!discovered[node]) {            //if new child, add to queue
                  parent[node] = start;            //remember parents, for paths
                  depthFirstSearch(node);
               }
            }
         } catch (NullPointerException e) {
            throw new RuntimeException(e + ": NO SUCH ELEMENT");
         }

         sort.push(start);
      }

      /*
       * Calculates the longest distance between vertexes from
       * @param start   to   @param goal.
       * @ creates longest path
       */
      public void calculateLongestDistance(int start, int goal) {
         //initiate DFS and topological sort
         discovered = new boolean[adj.size() + 1];
         parent = new int[adj.size() + 1];
         sort = new ArrayDeque<>();

         //do topological sort
         depthFirstSearch(start);  // (1*)

            /* Compute the length of the longest path ending at goal by looking at its incoming
             neighbors and adding one to the maximum length recorded for those neighbors. */
         int[] longestDistance = new int[adj.size() + 1];  // (2*)
         for (int from : sort)
            for (int to : adj.get(from))
               longestDistance[to] = Math.max(longestDistance[to], longestDistance[from] + 1);

         longestPath.add(start);
         createPath(longestDistance, start, goal);  // (3*)
      }

      /*
       *@param longestDistance
       *@param start
       *@param endPoint
       *@Puts longest path into global variable "longestPath"
       */
      public void createPath(int[] longestDistance, int start, int endPoint) {
         if (endPoint == start) {
            return;
         }
         //find back-edge of highest length
         int nextDistance = -1;
         int next = start;
         try {
            for (int i = 0; i < adj.size(); i++) {
               if (adj.get(i).contains(endPoint) && longestDistance[i] > nextDistance) {
                  nextDistance = longestDistance[i];
                  next = i;
               }
            }
         } catch (NullPointerException e) {
            throw new RuntimeException("v" + start + " cant get to v" + endPoint);
         }


         createPath(longestDistance, start, next);
         longestPath.add(endPoint);
      }


      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         for (int i = 0; i < n; i++) {
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createEdge(i, vnr);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = firstGraphVertex;
         while (v != null) {
            v.info = info++;
            v = v.nextVertex;
         }
         int[][] res = new int [info][info];
         v = firstGraphVertex;
         while (v != null) {
            int i = v.info;
            Arc a = v.vertexArc;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.nextVertex;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         firstGraphVertex = null;
         createRandomTree (n);
         Vertex[] vert = new Vertex [n];
         Vertex v = firstGraphVertex;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.nextVertex;
         }

         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         int count = 0; // count losted arcs , it will happen, if user asks too much arcs, not working at well
         int times = n*(n-1)/2;
         while (edgeCount > 0) {
            times -= 1;
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            try {
               createEdge(i, j);
               connected [i][j] = 1;
               times = n*(n-1)/2;
               edgeCount--;  // a new edge happily created
            } catch (RuntimeException e){
               if (times <= 0) {
                  edgeCount--;  // arc was not created
                  count++;
               }
            }

         }
         System.out.println("MISSED " + count + " ARCS");
      }


   }

}